<?php
$first_names = array('ali','ahmad','pedraam','mahmood','alireza','barbod');
$last_names = array('bayati','moradi','farahani','dadkhah','mohammadi','salami');
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Loops</title>
</head>
<body>
<?php foreach ($first_names as $key=>$value): ?>
	<p><?php echo $value .' - ' . $last_names[$key]; ?></p>
<?php endforeach; ?>
</body>
</html>
